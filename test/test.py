from bitbucket_pipes_toolkit.test import PipeTestCase
import os
import subprocess
import uuid
import tarfile
import re

import pytest
import requests
from docker.errors import ContainerError


docker_image = 'bitbucketpipelines/demo-pipe-python:ci' + \
    os.getenv('BITBUCKET_BUILD_NUMBER', 'local')


class HerokuDeployTestCase(PipeTestCase):

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.build_id = uuid.uuid4()
        self.dirname = os.path.dirname(__file__)

        with tarfile.open(os.path.join(self.dirname, 'sample-app.tar.gz'), "w:gz") as tar:
            source_dir =  os.path.join(self.dirname, 'python-getting-started-master')
            tar.add(source_dir, arcname=os.path.basename(source_dir))

    def tearDown(self):
        os.remove(os.path.join(self.dirname, 'sample-app.tar.gz'))

    def test_no_parameters(self):
        with self.assertRaises(ContainerError) as error:
            self.run_container()
        self.assertIn('HEROKU_API_KEY variable missing',
                      error.exception.stderr.decode())

    @pytest.mark.last
    def test_app_deployed_successfully_default(self):
        cwd = os.getcwd()
        app_name = os.getenv('HEROKU_APP_NAME')
        result = self.run_container(environment={
            'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
            'HEROKU_APP_NAME': app_name,
            'ZIP_FILE': 'test/sample-app.tar.gz'
            },
            volumes={cwd: {'bind': cwd, 'mode': 'rw'}},
            working_dir=cwd)
        self.assertIn(
            'Successfully started a new build', result.decode())

    def test_app_deployed_successfully_wait(self):
        cwd = os.getcwd()
        result = self.run_container(environment={
            'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
            'HEROKU_APP_NAME': os.getenv('HEROKU_APP_NAME'),
            'ZIP_FILE': 'test/sample-app.tar.gz',
            'WAIT': True
            },
            volumes={cwd: {'bind': cwd, 'mode': 'rw'}},
            working_dir=cwd)
        self.assertIn(
            'Successfully deployed the application to Heroku', result.decode())

    def test_app_deployment_fails_with_invalid_package(self):
        cwd = os.getcwd()
        with self.assertRaises(ContainerError) as error:
            self.run_container(environment={
                'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
                'HEROKU_APP_NAME': os.getenv('HEROKU_APP_NAME'),
                'ZIP_FILE': 'test/invalid-package.tar.gz',
                'WAIT': True,
                },
                volumes={cwd: {'bind': cwd, 'mode': 'rw'}},
                working_dir=cwd)
        self.assertIn('Heroku build failed!', error.exception.container.logs().decode())

    def test_deployment_fails_if_app_access_denied(self):

        app_name='pipes-ci-aaccess-denied'
        cwd = os.getcwd()
        with self.assertRaises(ContainerError) as error:
            self.run_container(environment={
                'HEROKU_API_KEY': os.getenv('HEROKU_API_KEY'),
                'HEROKU_APP_NAME': app_name,
                'ZIP_FILE': 'test/invalid-package.tar.gz',
                'WAIT': True,
                },
                volumes={cwd: {'bind': cwd, 'mode': 'rw'}},
                working_dir=cwd)
        self.assertIn(f'You do not have access to the app {app_name}', error.exception.container.logs().decode())
